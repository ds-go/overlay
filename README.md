# Overlay

Load golang templates from directory and overlay them with layouts/other templates.

```
Template, err := overlay.NewTheme(
    "master.tpl", 
    filepath.Join(filepath.Dir("."), "example", "views"),
    "layouts",
)
if err != nil {
    panic(err)
}

//page.tpl extends master.tpl

type pageData struct{
    field string
}

data := pageData{
    field: "hello world!",
}

Template.Render(w, "layouts/page.tpl", data)

```